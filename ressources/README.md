# Ressources

## Ollama

Il est possible de faire fonctionner Ollama à l'aide de Docker, la reste une des façons les plus simple pour l'installer.

```bash
docker run -d -v ollama:/root/.ollama -p 11434:11434 --name ollama ollama/ollama
```

Ensuite il est possible de lancer le modèle de son choix : 

```bash
docker exec ollama ollama run tinydolphin
```

Ici c'est le modèle **tinydolphin** qui est lancé, mais vous pouvez aussi en lancer un autre comme llama.
Les modèles sont disponibles [ICI](https://ollama.com/library).

> Attention au modèle que vous séléctionnez ... certains peuvent être très volumineux .. pensez à vos besoins, pensez à la planète 🌳.

## PgVector

Une base de données Postgresql avec l'extension pgvector est nécessaire pour certains exemples. Le dossier [pgvector](pgvector/) contient ce qu'il faut pour builder l'image et lancer le container.