package main

import (
	"context"
	"log"
	"os"
	"strings"

	"github.com/tmc/langchaingo/chains"
	"github.com/tmc/langchaingo/documentloaders"
	"github.com/tmc/langchaingo/llms/ollama"
	"github.com/tmc/langchaingo/textsplitter"
)

func main() {

	//create LLM with llama3 model
	llm, err := ollama.New(ollama.WithModel("llama3"))
	if err != nil {
		log.Fatal(err)
	}

	stuffQAChain := chains.LoadStuffQA(llm)

	dat, err := os.ReadFile("inputs.txt")
	others, errtext := documentloaders.NewText(strings.NewReader(string(dat))).LoadAndSplit(context.Background(), textsplitter.NewRecursiveCharacter())

	if errtext != nil {
		log.Fatal(err)
	}

	log.Default().Println("Prompt : Which region contains more of users ?")
	answer, err := chains.Call(context.Background(), stuffQAChain, map[string]any{
		"input_documents": others,
		"question":        "Which region contains more of users ?",
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Default().Println(answer)
}
