package main

import (
	"context"
	"log"

	"github.com/tmc/langchaingo/llms"
	"github.com/tmc/langchaingo/llms/ollama"
)

func main() {

	ctx := context.Background()

	//create LLM with tinydolphin model
	llm, err := ollama.New(ollama.WithModel("tinydolphin"))
	if err != nil {
		log.Fatal(err)
	}

	log.Default().Println("Prompt : Which region contains more of users ?")
	prompt := "Which region contains more of users ?"
	completion, err := llms.GenerateFromSinglePrompt(ctx, llm, prompt)
	if err != nil {
		log.Fatal(err)
	}
	log.Default().Println(completion)
}
