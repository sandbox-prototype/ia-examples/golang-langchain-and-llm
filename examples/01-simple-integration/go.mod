module simple-integration

go 1.22.3

require (
	github.com/dlclark/regexp2 v1.10.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/pkoukk/tiktoken-go v0.1.6 // indirect
	github.com/tmc/langchaingo v0.1.12 // indirect
)
