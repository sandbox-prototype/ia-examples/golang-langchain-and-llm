package main

import (
	"context"
	"log"
	"os"

	"github.com/tmc/langchaingo/documentloaders"
	"github.com/tmc/langchaingo/embeddings"
	"github.com/tmc/langchaingo/llms/ollama"
	"github.com/tmc/langchaingo/schema"
	"github.com/tmc/langchaingo/vectorstores/pgvector"
)

func main() {
	ctx := context.Background()
	docs := fetchDocuments(ctx)

	//model initialization
	llms, err := ollama.New(ollama.WithModel("llama3"))
	if err != nil {
		log.Fatal("Error during LLM initialization: ", err)
	}

	//embedder initialization
	e, err := embeddings.NewEmbedder(llms)
	if err != nil {
		log.Fatal("Error during Embeder initialization: ", err)
	}

	// connection to the database
	store, err := pgvector.New(
		ctx,
		pgvector.WithConnectionURL("postgres://testuser:password@localhost:5432/testdb?sslmode=disable"),
		pgvector.WithEmbedder(e),
	)
	if err != nil {
		log.Fatal("Error during PgVector initialization: ", err)
	}

	// store vectors !
	log.Println("Store data (vectors) on the database")
	store.AddDocuments(context.Background(), docs)

	// verification of storage by searching by similarity
	test, err := store.SimilaritySearch(ctx, "only document about women or developper", 5)

	if err != nil {
		log.Fatal(err)
	}

	log.Println(test)

}

func fetchDocuments(ctx context.Context) []schema.Document {
	// load documents from PDF files
	docsFromPdf := fetchDocumentsFromPdf("./files/example.pdf", ctx)
	log.Println("Number of documents loaded from the pdf file: ", len(docsFromPdf))
	log.Println("Metatada in documents loaded from the pdf file: ", docsFromPdf[0].Metadata)
	log.Println("----------------------------------------------------------------------------")
	docsFromOtherSource := fetchDocumentsFromWhatYouWant()
	log.Println("Number of documents loaded from the pdf file: ", len(docsFromOtherSource))
	log.Println("Metatada in documents loaded from the pdf file: ", docsFromOtherSource[0].Metadata)

	docsFromPdf = append(docsFromPdf, docsFromOtherSource...)
	return docsFromPdf
}

func fetchDocumentsFromPdf(filepath string, ctx context.Context) []schema.Document {
	log.Println("Fetch document from this file: ", filepath)
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatalln("Error opening file: ", err)
	}

	fileStat, err := file.Stat()
	if err != nil {
		log.Fatalln("Error while retrieving file information: ", err)
	}
	pdfDocuments := documentloaders.NewPDF(file, fileStat.Size())

	docs, err := pdfDocuments.Load(ctx)

	if err != nil {
		log.Fatalln("Error loading document: ", err)
	}

	return docs
}

func fetchDocumentsFromWhatYouWant() []schema.Document {
	docs := []schema.Document{
		{
			PageContent: `Anthony is a developer from Brittany (where it never rains)!
			He tinkers with things like prototypes in Golang, IOT, Kubernetes.
			Beer and aperitifs are his friends. Anthony is a boy.
			`,
			Metadata: map[string]any{
				"source":      "fetchDocumentsFromWhatYouWant",
				"source_date": "202407",
			},
		},
		{
			PageContent: `Radia Perlman, born in 1951, is a computer programmer and network engineer.
			As one of the most influential women in computer science, she has made significant contributions to the field.
			`,
			Metadata: map[string]any{
				"source":      "fetchDocumentsFromWhatYouWant",
				"source_date": "202407",
			},
		},
	}

	return docs
}
