package main

import (
	"context"
	"log"

	"github.com/tmc/langchaingo/chains"
	"github.com/tmc/langchaingo/embeddings"
	"github.com/tmc/langchaingo/llms/ollama"
	"github.com/tmc/langchaingo/vectorstores"
	"github.com/tmc/langchaingo/vectorstores/pgvector"
)

func main() {

	ctx := context.Background()
	llm, err := ollama.New(ollama.WithModel("tinydolphin"))
	if err != nil {
		log.Fatal("Error during LLM initialization: ", err)
	}
	e, err := embeddings.NewEmbedder(llm)
	if err != nil {
		log.Fatal("Error during Embeder initialization: ", err)
	}

	store, err := pgvector.New(
		ctx,
		pgvector.WithConnectionURL("postgres://testuser:password@localhost:5432/testdb?sslmode=disable"),
		pgvector.WithEmbedder(e),
	)
	if err != nil {
		log.Fatal("Error during PgVector initialization: ", err)
	}

	//without filters or other options
	//vectorstores.WithScoreThreshold(0)
	docs, err := store.SimilaritySearch(ctx, "document about developer, golang or brittany", 7)
	if err != nil {
		log.Fatal("Error during SimilaritySearch : ", err)
	}

	stuffQAChain := chains.LoadStuffQA(llm)
	answer, err := chains.Call(context.Background(), stuffQAChain, map[string]any{
		"input_documents": docs,
		"question":        "Who is Anthony ?",
	})
	if err != nil {
		log.Fatal(err)
	}
	log.Println(answer)

	//with filter
	filter := map[string]any{"source": "fetchDocumentsFromWhatYouWant"}

	docs, err = store.SimilaritySearch(ctx, "document about developer, golang or brittany", 7, vectorstores.WithFilters(filter))
	if err != nil {
		log.Fatal("Error during SimilaritySearch : ", err)
	}

	log.Println("Number of documents with the filter: ", len(docs))

	answer, err = chains.Call(context.Background(), stuffQAChain, map[string]any{
		"input_documents": docs,
		"question":        "Who is Anthony ?",
	})
	if err != nil {
		log.Fatal(err)
	}
	log.Println(answer)

}
