# Golang, Langchain and LLM

Ce repository comprend une série d'exemple d'intégration de LLM avec Langchain en Golang.

Il est possible de lancer les différents projets à travers ![Static Badge](https://img.shields.io/badge/Gitpod-ffae33)
.

## Exemples

### Intégration simple

Cet exemple présente une intégration de Lanchain dans un programme Golang.
Le LLM utilisé derrière tourne sur Ollama .

### Intégration avec utilisation de documents dans le contexte

Cet exemple présente une intégration de Lanchain dans un programme Golang.
L'exemple utilise l'intégration de document dans le contexte afin cibler les réponses du LLM.
Le LLM utilisé derrière tourne sur Ollama .


## Ressources

Certains exemples ont besoins de ressources complémentaires (LLM, base de données). Vous pouvez retrouver ces ressources dans [le dossier /ressources du projet](/ressources).



## Quelques liens

- Langchain : Framework facilitant l'intégration de LLM dans vos applications
    - [Documentation ici](https://www.langchain.com/)
- Langchaingo : Implémentation de Langchain pour Golang
    - [Documentation ici](https://tmc.github.io/langchaingo/docs/)
- Ollama : Moteur LLM qui fonctionne en local
    - [Documentation ici](https://ollama.com/)